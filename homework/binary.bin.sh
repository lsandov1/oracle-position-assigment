#!/usr/bin/env bash

set -e

BINARY="${BINARY:-./binary.upd.bin}"
LIST="${LIST:-list.small}"

# in case of no parameters, run the standalone binary as ${BINARY} < ${LIST}
PARALLEL="${PARALLEL:-+}"

# if at least one argument is present, take the first one as # of threads parameter with
# the following syntax
#   '+'      run the standalone binary as ${BINARY} < ${LIST}
#   '-jN'    Run the binary in parallel 

if [ "$#" -gt 0 ]; then
    PARALLEL="$1"
fi

if [ "${PARALLEL}" = "+" ]; then
    ${BINARY} < ${LIST}
else
    make ${PARALLEL} BINARY=${BINARY} LIST=${LIST}
fi
